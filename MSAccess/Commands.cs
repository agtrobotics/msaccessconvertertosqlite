﻿using JointCriteriaDatabase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSAccess
{
    public class Commands
    {
        OleDbCommand command;
        OleDbConnection connection;

        public Commands(string msAccessPath)
        {
            ConnectTo(msAccessPath);   
        }

        private void ConnectTo(string msAccessPath)
        {
            connection = new OleDbConnection(
                $"Provider=Microsoft.ACE.OLEDB.15.0;Data Source={msAccessPath};Persist Security Info=False");
            command = connection.CreateCommand();
        }

        public List<JointCriteria> GetAllJointFromAccess()
        {
            connection.Open();
            List<JointCriteria> joints = GetTableMachineCondCriteria();
            CompleteMissingInfoFromCondCriteriaPropertyValues(joints);
            return joints;
        }

        private void CompleteMissingInfoFromCondCriteriaPropertyValues(List<JointCriteria> joints)
        {
            command.CommandText = "SELECT tbl_OWE_Properties.OWE_PropertyAlias, tbl_MachinesCondCriteriaPropertyValues.OWE_PropertyValue, tbl_MachinesCondCriteriaPropertyValues.OWE_ConditionalCriteriaFCK FROM tbl_OWE_Properties INNER JOIN tbl_MachinesCondCriteriaPropertyValues ON tbl_OWE_Properties.OWE_PropertyPCK = tbl_MachinesCondCriteriaPropertyValues.OWE_PropertyFCK;";
            command.CommandType = CommandType.Text;

            using (OleDbDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    string propertyName = reader["OWE_PropertyAlias"].ToString();
                    string value = reader["OWE_PropertyValue"].ToString();
                    long id = Convert.ToInt64(reader["OWE_ConditionalCriteriaFCK"].ToString());
                    JointCriteria joint = joints.FirstOrDefault(x => x.ID == id);
                    if (propertyName == "General / StartWeldOffset")
                    {
                        joint.StartWeldOffset = Convert.ToDouble(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "Process / StartJobModeNumber")
                    {
                        joint.StartJobModeNumber = Convert.ToInt64(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "General / EndWeldOffset")
                    {
                        joint.EndWeldOffset = Convert.ToDouble(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "General / Push(-)/Pull(+)")
                    {
                        joint.PushAngle = Convert.ToDouble(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "General / ShiftFromFloor")
                    {
                        joint.ShiftFromFloor = Convert.ToDouble(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "General / ShiftFromWall")
                    {
                        joint.ShiftFromWall = Convert.ToDouble(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "General / TorchTilt")
                    {
                        joint.ElevationAngle = Convert.ToDouble(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "Process / StickOutOffset")
                    {
                        joint.StickOutOffset = Convert.ToDouble(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "Process / TransitionNumber")
                    {
                        joint.TransitionNumber = Convert.ToInt64(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "Process / WeaveAmplitude")
                    {
                        joint.WeaveAmplitude = Convert.ToDouble(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "Process / WeaveLeftSideDelay")
                    {
                        joint.WeaveLeftSideDelay = Convert.ToDouble(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "Process / WeaveLength")
                    {
                        joint.WeaveLengthFrequency_ = Convert.ToDouble(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "Process / WeaveAmplitude")
                    {
                        joint.WeaveAmplitude = Convert.ToDouble(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "Process / WeavePatternType")
                    {
                        if (value == "None")
                            joint.WeavePatternType = 1;
                        else if (value == "Sine")
                            joint.WeavePatternType = 2;
                        else if (value == "Circle")
                            joint.WeavePatternType = 3;
                        else if (value == "Trapecoid")
                            joint.WeavePatternType = 4;
                        else
                            joint.WeavePatternType = 1;
                    }
                    else if (propertyName == "Process / WeaveRightSideDelay")
                    {
                        joint.WeaveRightSideDelay = Convert.ToDouble(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "Process / WeldingSpeed")
                    {
                        joint.WeldingSpeed = Convert.ToDouble(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "Process / WeldJobModeNumber")
                    {
                        joint.WeldJobModeNumber = Convert.ToInt64(value == string.Empty ? "0" : value);
                    }
                    else if (propertyName == "Process / CraterJobModeNumber")
                    {
                        joint.CraterJobModeNumber = Convert.ToInt64(value == string.Empty ? "0" : value);
                    }
                }
            }
        }

        private List<JointCriteria> GetTableMachineCondCriteria()
        {
            List<JointCriteria> joints = new List<JointCriteria>();
            command.CommandText = "SELECT * FROM tbl_MachinesCondCriteria";
            command.CommandType = CommandType.Text;

            using (OleDbDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    JointCriteria joint = new JointCriteria();
                    joint.ID = Convert.ToInt64(reader["OWE_ConditionalCriteriaPCK"].ToString());
                    joint.RobotType = 1;
                    joint.WeldingSource = 1;
                    joint.GroupCondition = Convert.ToInt64(reader["GroupConditionFK"].ToString() == string.Empty ? null : reader["GroupConditionFK"].ToString());
                    joint.JointType = Convert.ToInt64(reader["JointTypeCondFK"].ToString() == string.Empty ? "0" : reader["JointTypeCondFK"].ToString());
                    joint.ThicknessBaseMinCond = Convert.ToDouble(reader["ThicknessBaseMinCond"].ToString() == string.Empty ? "0" : reader["ThicknessBaseMinCond"].ToString());
                    joint.ThicknessBaseMaxCond = Convert.ToDouble(reader["ThicknessBaseMaxCond"].ToString() == string.Empty ? "0" : reader["ThicknessBaseMaxCond"].ToString());
                    joint.ThicknessWallMinCond = Convert.ToDouble(reader["ThicknessWallMinCond"].ToString() == string.Empty ? "0" : reader["ThicknessWallMinCond"].ToString());
                    joint.ThicknessWallMaxCond = Convert.ToDouble(reader["ThicknessWallMaxCond"].ToString() == string.Empty ? "0" : reader["ThicknessWallMaxCond"].ToString());
                    joint.JointAngleToHorizontalRightMinCond = Convert.ToDouble(reader["JointAngleToHorizontalRightMinCond"].ToString() == string.Empty ? "0" : reader["JointAngleToHorizontalRightMinCond"].ToString());
                    joint.JointAngleToHorizontalRightMaxCond = Convert.ToDouble(reader["JointAngleToHorizontalRightMaxCond"].ToString() == string.Empty ? "0" : reader["JointAngleToHorizontalRightMaxCond"].ToString());
                    joint.SurfaceFinishOfWeldBaseCond = Convert.ToInt64(reader["SurfaceFinishOfWeldBaseCondFK"].ToString() == string.Empty ? null : reader["SurfaceFinishOfWeldBaseCondFK"].ToString());
                    joint.SurfaceFinishOfWeldWallCond = Convert.ToInt64(reader["SurfaceFinishOfWeldWallCondFK"].ToString() == string.Empty ? null : reader["SurfaceFinishOfWeldWallCondFK"].ToString());
                    joint.WeldLegSizeMinCond = Convert.ToDouble(reader["WeldLegSizeMinCond"].ToString() == string.Empty ? "0" : reader["WeldLegSizeMinCond"].ToString());
                    joint.WeldLegSizeMaxCond = Convert.ToDouble(reader["WeldLegSizeMaxCond"].ToString() == string.Empty ? "0" : reader["WeldLegSizeMaxCond"].ToString());
                    joint.MaxGapValueMinCond = Convert.ToDouble(reader["MaxGapValueMinCond"].ToString() == string.Empty ? "0" : reader["MaxGapValueMinCond"].ToString());
                    joint.MaxGapValueMaxCond = Convert.ToDouble(reader["MaxGapValueMaxCond"].ToString() == string.Empty ? "0" : reader["MaxGapValueMaxCond"].ToString());
                    joint.GapNominalMinCond = Convert.ToDouble(reader["GapNominalMinCond"].ToString() == string.Empty ? "0" : reader["GapNominalMinCond"].ToString());
                    joint.GapNominalMaxCond = Convert.ToDouble(reader["GapNominalMaxCond"].ToString() == string.Empty ? "0" : reader["GapNominalMaxCond"].ToString());
                    joint.PartsInterfacePlaneOrientationMinCond = Convert.ToDouble(reader["PartsInterfacePlaneOrientationMinCond"].ToString() == string.Empty ? "0" : reader["PartsInterfacePlaneOrientationMinCond"].ToString());
                    joint.PartsInterfacePlaneOrientationMaxCond = Convert.ToDouble(reader["PartsInterfacePlaneOrientationMaxCond"].ToString() == string.Empty ? "0" : reader["PartsInterfacePlaneOrientationMaxCond"].ToString());
                    joint.WeldLengthMinCond = Convert.ToDouble(reader["WeldLengthMinCond"].ToString() == string.Empty ? "0" : reader["WeldLengthMinCond"].ToString());
                    joint.WeldLengthMaxCond = Convert.ToDouble(reader["WeldLengthMaxCond"].ToString() == string.Empty ? "0" : reader["WeldLengthMaxCond"].ToString());
                    joint.JointWallHeightMinCond = Convert.ToDouble(reader["JointWallHeightMinCond"].ToString() == string.Empty ? "0" : reader["JointWallHeightMinCond"].ToString());
                    joint.JointWallHeightMaxCond = Convert.ToDouble(reader["JointWallHeightMaxCond"].ToString() == string.Empty ? "0" : reader["JointWallHeightMaxCond"].ToString());
                    joint.JointEdgeRadiusMinCond = Convert.ToDouble(reader["JointEdgeRadiusMinCond"].ToString() == string.Empty ? "0" : reader["JointEdgeRadiusMinCond"].ToString());
                    joint.JointEdgeRadiusMaxCond = Convert.ToDouble(reader["JointEdgeRadiusMaxCond"].ToString() == string.Empty ? "0" : reader["JointEdgeRadiusMaxCond"].ToString());
                    joint.JointFloorWidthMinCond = Convert.ToDouble(reader["JointFloorWidthMinCond"].ToString() == string.Empty ? "0" : reader["JointFloorWidthMinCond"].ToString());
                    joint.JointFloorWidthMaxCond = Convert.ToDouble(reader["JointFloorWidthMaxCond"].ToString() == string.Empty ? "0" : reader["JointFloorWidthMaxCond"].ToString());
                    joint.TrajectoryEdgeRadiusMinCond = Convert.ToDouble(reader["TrajectoryEdgeRadiusMinCond"].ToString() == string.Empty ? "0" : reader["TrajectoryEdgeRadiusMinCond"].ToString());
                    joint.TrajectoryEdgeRadiusMaxCond = Convert.ToDouble(reader["TrajectoryEdgeRadiusMaxCond"].ToString() == string.Empty ? "0" : reader["TrajectoryEdgeRadiusMaxCond"].ToString());
                    joint.EdgeTypeCond = Convert.ToInt64(reader["EdgeTypeCond"].ToString() == string.Empty ? null : reader["EdgeTypeCond"].ToString());
                    joint.JointEnclosureStartHeightMinCond = Convert.ToDouble(reader["JointEnclosureStartHeightMinCond"].ToString() == string.Empty ? "0" : reader["JointEnclosureStartHeightMinCond"].ToString());
                    joint.JointEnclosureStartHeightMaxCond = Convert.ToDouble(reader["JointEnclosureStartHeightMaxCond"].ToString() == string.Empty ? "0" : reader["JointEnclosureStartHeightMaxCond"].ToString());
                    joint.JointEnclosureStopHeightMinCond = Convert.ToDouble(reader["JointEnclosureStopHeightMinCond"].ToString() == string.Empty ? "0" : reader["JointEnclosureStopHeightMinCond"].ToString());
                    joint.JointEnclosureStopHeightMaxCond = Convert.ToDouble(reader["JointEnclosureStopHeightMaxCond"].ToString() == string.Empty ? "0" : reader["JointEnclosureStopHeightMaxCond"].ToString());
                    joint.JointEnclosureFrontAngleMinCond = Convert.ToDouble(reader["JointEnclosureFrontAngleMinCond"].ToString() == string.Empty ? "0" : reader["JointEnclosureFrontAngleMinCond"].ToString());
                    joint.JointEnclosureFrontAngleMaxCond = Convert.ToDouble(reader["JointEnclosureFrontAngleMaxCond"].ToString() == string.Empty ? "0" : reader["JointEnclosureFrontAngleMaxCond"].ToString());
                    joint.DistanceBeyondStartOfJointOnFloorMinCond = Convert.ToDouble(reader["DistanceBeyondStartOfJointOnFloorMinCond"].ToString() == string.Empty ? "0" : reader["DistanceBeyondStartOfJointOnFloorMinCond"].ToString());
                    joint.DistanceBeyondStartOfJointOnFloorMaxCond = Convert.ToDouble(reader["DistanceBeyondStartOfJointOnFloorMaxCond"].ToString() == string.Empty ? "0" : reader["DistanceBeyondStartOfJointOnFloorMaxCond"].ToString());
                    joint.DistanceBeyondStartOfJointOnWallMinCond = Convert.ToDouble(reader["DistanceBeyondStartOfJointOnWallMinCond"].ToString() == string.Empty ? "0" : reader["DistanceBeyondStartOfJointOnWallMinCond"].ToString());
                    joint.DistanceBeyondStartOfJointOnWallMaxCond = Convert.ToDouble(reader["DistanceBeyondStartOfJointOnWallMaxCond"].ToString() == string.Empty ? "0" : reader["DistanceBeyondStartOfJointOnWallMaxCond"].ToString());
                    joint.DistanceBeyondStopOfJointOnFloorMinCond = Convert.ToDouble(reader["DistanceBeyondStopOfJointOnFloorMinCond"].ToString() == string.Empty ? "0" : reader["DistanceBeyondStopOfJointOnFloorMinCond"].ToString());
                    joint.DistanceBeyondStopOfJointOnFloorMaxCond = Convert.ToDouble(reader["DistanceBeyondStopOfJointOnFloorMaxCond"].ToString() == string.Empty ? "0" : reader["DistanceBeyondStopOfJointOnFloorMaxCond"].ToString());
                    joint.DistanceBeyondStopOfJointOnWallMinCond = Convert.ToDouble(reader["DistanceBeyondStopOfJointOnWallMinCond"].ToString() == string.Empty ? "0" : reader["DistanceBeyondStopOfJointOnWallMinCond"].ToString());
                    joint.DistanceBeyondStopOfJointOnWallMaxCond = Convert.ToDouble(reader["DistanceBeyondStopOfJointOnWallMaxCond"].ToString() == string.Empty ? "0" : reader["DistanceBeyondStopOfJointOnWallMaxCond"].ToString());
                    joint.WallFloorApertureAngleMinCond = Convert.ToDouble(reader["WallFloorApertureAngleMinCond"].ToString() == string.Empty ? "0" : reader["WallFloorApertureAngleMinCond"].ToString());
                    joint.WallFloorApertureAngleMaxCond = Convert.ToDouble(reader["WallFloorApertureAngleMaxCond"].ToString() == string.Empty ? "0" : reader["WallFloorApertureAngleMaxCond"].ToString());
                    joint.JointCeilingStartHeightMinCond = Convert.ToDouble(reader["JointCeilingStartHeightMinCond"].ToString() == string.Empty ? "0" : reader["JointCeilingStartHeightMinCond"].ToString());
                    joint.JointCeilingStartHeightMaxCond = Convert.ToDouble(reader["JointCeilingStartHeightMaxCond"].ToString() == string.Empty ? "0" : reader["JointCeilingStartHeightMaxCond"].ToString());
                    joint.JointCeilingEndHeightMinCond = Convert.ToDouble(reader["JointCeilingEndHeightMinCond"].ToString() == string.Empty ? "0" : reader["JointCeilingEndHeightMinCond"].ToString());
                    joint.JointCeilingEndHeightMaxCond = Convert.ToDouble(reader["JointCeilingEndHeightMaxCond"].ToString() == string.Empty ? "0" : reader["JointCeilingEndHeightMaxCond"].ToString());
                    joint.AGroupCond = reader["AGroupCond"].ToString();
                    joint.ConditionDescription = reader["ConditionDescription"].ToString();
                    joint.PassNumber = Convert.ToInt64(reader["PassNumberMinCond"].ToString() == string.Empty ? "0" : reader["PassNumberMinCond"].ToString());
                    joint.PassNumberTotal = Convert.ToInt64(reader["PassNumberMaxCond"].ToString() == string.Empty ? "0" : reader["PassNumberMaxCond"].ToString());
                    joint.WeavePatternType = 1;
                    joint.CurveType = "None";
                    joint.WallThicknessMin = 0;
                    joint.WallThicknessMax = 999;
                    joint.FloorThicknessMin = 0;
                    joint.FloorThicknessMax = 999;
                    joint.Active = 1;
                    joints.Add(joint);
                }
            }
            return joints;
        }
    }
}
