﻿using JointCriteriaDatabase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSAccess
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Convert tool for the database OWE_Properties Access to SQLite.");
            Console.WriteLine("Enter the path of OWE_Properties.mdb :");
            string accessFile = Console.ReadLine() + "\\OWE_Properties.mdb";

            while (!File.Exists(accessFile))
            {
                Console.WriteLine("No file found at {0}, enter the path of OWE_Properties.mdb :", accessFile);
                accessFile = Console.ReadLine() + "\\OWE_Properties.mdb";
            }

            Console.WriteLine("Enter the path where you want the new Sqlite database :");
            string sqliteFile = Console.ReadLine();
            while (!Directory.Exists(sqliteFile))
            {
                Console.WriteLine("No folder found at {0}, enter the path where you want the sqlite database :", sqliteFile);
                sqliteFile = Console.ReadLine();
            }
            sqliteFile = sqliteFile + "\\JointCriteriaDB.db";
            File.Copy("..\\..\\..\\..\\JointCriteriaDB.db", sqliteFile,true);

            Commands commands = new Commands(accessFile);

            string connectionString = string.Format("metadata=res://*/JointCriteriaDatabaseModel.csdl|res://*/JointCriteriaDatabaseModel.ssdl|res://*/JointCriteriaDatabaseModel.msl;provider=System.Data.SQLite.EF6;provider connection string=\"data source={0}\"",sqliteFile);
            JointCriteriaDatabaseEntities context = new JointCriteriaDatabaseEntities(connectionString);
            List<JointCriteria> joints = commands.GetAllJointFromAccess();
            context.JointCriterias.AddRange(joints);
            context.SaveChanges();

            if (File.Exists(sqliteFile))
                Console.WriteLine("JointCriteriaDB have been created {0}", sqliteFile);
            else
                Console.WriteLine($"No file have been created {sqliteFile}, check your path and try again.");

            Console.ReadLine();
        }
    }
}
